<?php

namespace CodeArtisan\LaravelCategories;

use Illuminate\Support\ServiceProvider;

class CategoriesServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $timestamp = date('Y_m_d_His');
        $this->publishes([
            __DIR__.'/../database/migrations/create_category_tables.php' => database_path("migrations/{$timestamp}_create_category_tables.php"),
        ], 'codeartisan-categories');
    }
}
