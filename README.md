# Laravel Categories

This package adds categories to your Laravel project.

It uses [polymorphic relations](https://laravel.com/docs/5.8/eloquent-relationships#polymorphic-relationships) which allows you to use it with any model as long as you include the `Categorizable` trait.

## Installation

```
$ composer require codeartisan/laravel-categories
```

## Publish Migrations And Configurations

```
$ php artisan vendor:publish --tag codeartisan-categories
```

## How To Use It

### Enable Categories On A Model

```php
<?php
...
use CodeArtisan\LaravelCategories\Categorizable;
...
class BlogPost extends Model
{
    use Categorizable;
    ...
}
```

### Create Categories

```php
Category::create(['name' => 'New category']);
```

### Attach Categories To A Model

```php
$post = BlogPost::find(1);
$post->categories()->sync([1,2,3]);
```

### Get Categories Linked To A Model

```php
$post = BlogPost::find(1);
dd($post->categories);
```

## Future Plans

* Automaically create categories that doesn't exist
* Adding slugs
* Adding translations
* Enable nested categories

## Changelog

**2019-08-22**

* Basic functionality to enable categories
